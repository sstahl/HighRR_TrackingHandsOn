#ifndef INCLUDE_TFRMCHIT_H
#define INCLUDE_TFRMCHIT_H 1

/*!
 *  @author    Alessio Piucci
 *  @brief     Representation of a MC hit
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//custom libraries
#include "TFRLayer.h"

using namespace std;

class TFRMCHit : public TObject {

 public:

  /*! Empty constructor */
  TFRMCHit(){ };
  
  /*! Standard constructor */
  TFRMCHit(TFRLayer _layer, TVector3 _hit_position = TVector3(-999., -999., -999.)) :
    layer(_layer), hit_position(_hit_position) {}

  /*! Destructor */
  virtual ~TFRMCHit( ){ };

  /*! Set the hit position */
  inline void SetPosition(TVector3 _hit_position){hit_position = _hit_position;};
  
  /*! Get the layer */
  TFRLayer GetLayer(){return layer;};
  
  /*! Returns the hit position */
  const TVector3 GetPosition(){return hit_position;};
  
 protected:
    
 private:

  /*! Layer of the hit */
  TFRLayer layer;

  /*! Hit position */
  TVector3 hit_position;
  
  ClassDef(TFRMCHit, 1)
    
} ;

/*! Array of hits */
typedef TObjArray TFRMCHits;

#endif // INCLUDE_TFRMCHIT_H
