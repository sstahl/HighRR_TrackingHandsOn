#ifndef INCLUDE_TFRGEOMETRY_H
#define INCLUDE_TFRGEOMETRY_H 1

/*!
 *  @author    Alessio Piucci
 *  @brief     Library to handle the geometry simulation
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//Boost libraries
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

//ROOT libraries
#include "TROOT.h"
//#include "TEveTrackPropagator.h"

//custom libraries
#include "TFRMCHit.h"
#include "TFRCluster.h"
#include "TFRLayer.h"
#include "TFRMultipleScattering.h"

using namespace std;

class TFRGeometry {

 public:

  /*! Empty constructor */
  TFRGeometry(){ };
  
  /*! Standard constructor */
  TFRGeometry(const std::string configFile_name);
  
  /*! Destructor */
  ~TFRGeometry( );

  /*! Set the geometry tag */
  void SetTag(std::string _tag){tag = _tag;};
  
  /*! Set the detector geometry */
  void SetGeometry(const std::string configFile_name);

  /*! Clear the detector geometry */
  void ClearGeometry();
  
  /*! Get the detector layers */
  inline TFRLayers* GetLayers(){return layers;};

  /*! Teturns the geometry tag */
  inline std::string GetTag() const {return tag;};
  
  /*! Get the number of detector layers */
  inline unsigned int GetNLayers() const {return layers->GetEntries();};
  
  /*! Get the propagator */
  //inline TEveTrackPropagator* GetPropagator(){return propagator;};
  
  /*! Dump the detector geometry to a output stream */
  void DumpGeometry(std::ostream out_stream);

  /*! Dump the detector geometry to the std::cout stream */
  void DumpGeometry();
  
  /*! Retrieve the layer, by its index */
  TFRLayer* GetLayer(const unsigned int layer_ID);

  /*! Returns the layers between z_min and z_max */
  TFRLayers GetLayersBetween(const double z_min, const double z_max);

  /*! Set the magnetic field */
  inline void SetBField(const double _B_zmin, const double _B_zmax, const double _B_mag){
    B_zmin = _B_zmin;
    B_zmax = _B_zmax;
    B_mag = _B_mag;
  };
  
  /*! Returns the z_min of the magnetic field */
  inline double GetBFieldStartZ() const {return B_zmin;};

  /*! Returns the z_max of the magnetic field */
  inline double GetBFieldEndZ() const {return B_zmax;};

  /*! Returns the magnitude of the magnetic field */
  inline double GetBMag() const {return B_mag;};
  
  /*! Get the multiple scattering */
  inline TFRMultipleScattering* GetMultipleScattering(){return mult_scatt;}
    
  /*! Set the multiple scattering status (enabled/disabled) */
  inline void SetMultipleScattStatus(bool _multiple_scattering) {
    multiple_scattering = _multiple_scattering;}

  /*! Returns the multiple scattering status (enabled/disabled) */
  inline bool GetMultipleScattStatus() const {return multiple_scattering;};
  
 protected:
  
 private:

  /*! Geometry tag */
  std::string tag;
  
  /*! Detector layers */
  TFRLayers *layers;
  
  /*! Layer id --> layer map */
  std::map<unsigned int, TFRLayer*> layer_map;

  /*! z_min of the magnetic field */
  double B_zmin;

  /*! z_max of the magnetic field */
  double B_zmax;

  /*! Magnitude of the magnetic field */
  double B_mag;

  /*! Multiple scattering */
  TFRMultipleScattering *mult_scatt;
  
  /*! Status of the multiple scattering simulation (enabled/disabled) */
  bool multiple_scattering;
  
};

#endif // INCLUDE_TFRGEOMETRY_H
