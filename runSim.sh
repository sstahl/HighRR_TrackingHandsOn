
# source runTest.sh

#set the project directory variable, useful to handle some output/input locations
export PROJECT_DIR=$(pwd)

#compile everything
rm -rf build
mkdir build
cd build
cmake ../
make -j 4
cd ../

#create the output folder
#rm -rf output
mkdir output

#run the test: create a track and propagate it
#./build/bin/software_test -c config/config_test.info -o output/track_test.root

#run a plotting test
#root -l scripts/plot_test.cpp++\(\"$PROJECT_DIR/output/detector_hits.root\",\"track_list\"\)

###########

# tracking detector --> magnetic field --> tracking detector

#run the event generation
./build/bin/event_sim -e 100 -c config/event.info -o output/event_generation.root

#run the detector simulation
./build/bin/detector_sim -i output/event_generation.root -g config/config_geo_allpixel.info -o output/detector_hits_noBinDetector.root

#run the reconstruction
./build/bin/reconstruction -i output/detector_hits_noBinDetector.root -g config/config_geo_allpixel.info -o output/reconstruction_noBinDetector.root

#run the chi2 fitting
./build/bin/fitting_chi2_linear -i output/reconstruction_noBinDetector.root -g config/config_geo_allpixel.info -o output/chi2_fitting_noBinDetector.root

#run the kalman filter
./build/bin/kalmanfiltering -i output/chi2_fitting_noBinDetector.root -g config/config_geo_allpixel.info -o output/chi2_and_kalmanfiltered_noBinDetector.root

#make the output ntuples for the chi2 fitting
./build/bin/make_ntuples -i output/chi2_and_kalmanfiltered_noBinDetector.root -o output/out_ntuples_fitted_noBinDetector.root

###########

# tracking detector --> tracking detector in B

#run the detector simulation
./build/bin/detector_sim -i output/event_generation.root -g config/config_geo_allpixel_withBinDetector.info -o output/detector_hits_withBinDetector.root

#run the reconstruction
./build/bin/reconstruction -i output/detector_hits_withBinDetector.root -g config/config_geo_allpixel_withBinDetector.info -o output/reconstruction_withBinDetector.root

#run the chi2 fitting
./build/bin/fitting_chi2_quadratic -i output/reconstruction_withBinDetector.root -g config/config_geo_allpixel_withBinDetector.info -o output/chi2_fitting_withBinDetector.root

#run the kalman filter
./build/bin/kalmanfiltering -i output/chi2_fitting_withBinDetector.root -g config/config_geo_allpixel_withBinDetector.info -o output/chi2Linear_and_kalmanfiltered_withBinDetector.root

#make the output ntuples
./build/bin/make_ntuples -i output/chi2Linear_and_kalmanfiltered_withBinDetector.root -o output/out_ntuples_fitted_withBinDetector.root

###########

# no B at all

#run the detector simulation
./build/bin/detector_sim -i output/event_generation.root -g config/config_geo_allpixel_noBatAll.info -o output/detector_hits_noBatAll.root

#run the reconstruction
./build/bin/reconstruction -i output/detector_hits_noBatAll.root -g config/config_geo_allpixel_noBatAll.info -o output/reconstruction_noBatAll.root

#run the chi2 fitting
./build/bin/fitting_chi2_linear -i output/reconstruction_noBatAll.root -g config/config_geo_allpixel_noBatAll.info -o output/chi2_fitting_noBatAll.root

#run the kalman filter
./build/bin/kalmanfiltering -i output/chi2_fitting_noBatAll.root -g config/config_geo_allpixel_noBatAll.info -o output/chi2_and_kalmanfiltered_noBatAll.root

#make the output ntuples for the chi2 fitting
./build/bin/make_ntuples -i output/chi2_and_kalmanfiltered_noBatAll.root -o output/out_ntuples_fitted_noBatAll.root

###########

# a lot of detector layers in B, mainly for B debugging

#ru the detector simulation
./build/bin/detector_sim -i output/event_generation.root -g config/config_geo_B_debug.info -o output/detector_hits_B_debug.root

#make the output ntuples
./build/bin/make_ntuples -i output/detector_hits_B_debug.root -o output/out_ntuples_B_debug.root

###########

#simulate a D0 decay
./build/bin/decay_sim -e 2000 -i config/D02Kpi_kinvar.root -o output/event_generation_D0decay.root

#run the detector simulation
./build/bin/detector_sim -i output/event_generation_D0decay.root -g config/config_geo_allpixel.info -o output/detector_hits_D0decay.root

#run the reconstruction
./build/bin/reconstruction -i output/detector_hits_D0decay.root -g config/config_geo_allpixel.info -o output/reconstruction_D0decay.root

#run the chi2 fitting
./build/bin/fitting_chi2_linear -i output/reconstruction_D0decay.root -g config/config_geo_allpixel.info -o output/chi2_fitting_D0decay.root

#run the Kalman filtering
./build/bin/kalmanfiltering -i output/chi2_fitting_D0decay.root -g config/config_geo_allpixel.info -o output/chi2_Kalman_fitting_D0decay.root

#reconstruct the D0 with chi2 reconstructed momentum
./build/bin/reco_decay -i output/chi2_Kalman_fitting_D0decay.root -g config/config_geo_allpixel.info -o output/decay_D0_chi2.root -k 0

#reconstruct the D0 with Kalman reconstructed momentum
./build/bin/reco_decay -i output/chi2_Kalman_fitting_D0decay.root -g config/config_geo_allpixel.info -o output/decay_D0_Kalman.root -k 1

#make the output ntuples
./build/bin/make_ntuples -i output/decay_D0_chi2.root -o output/out_ntuples_D0decay_chi2.root
./build/bin/make_ntuples -i output/decay_D0_Kalman.root -o output/out_ntuples_D0decay_kalman.root

###########

#to generate Doxygen documentation
#doxygen ./include/Doxyfile.in

#to navigate the documentation
#firefox index.html
