#ifndef INCLUDE_TFRKALMANFILTER_H
#define INCLUDE_TFRKALMANFILTER_H 1

/*!
 *  @author    Simon Stemmle
 *  @brief     Kalman filter to fit a TFRtrack
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//ROOT libraries
#include "TROOT.h"
#include "TMatrixD.h"
#include "TVectorD.h"

//custom libraries
#include "TFRPropagator.h"
#include "TFRTrack.h"

using namespace std;

class TFRKalmanFilter{
 public:

  /*! Empty constructor */
  TFRKalmanFilter(){ };
  
  /*! Standard constructor */
  TFRKalmanFilter(TFRPropagator *propagator):
   m_propagator(propagator) {};
  
  /*! Destructor */
  virtual ~TFRKalmanFilter( ){ };

  /*! Returns the propagator */
  TFRPropagator* GetPropagator() const {return m_propagator;};
  
  /*! Fits the track */
  void FitTrack(TFRTrack *track);
  
  /*! Create the first state (at the first measurement) to feed into the kalman filter */
  void CreateSeedState(TFRFitnodes *nodes, TFRTrack *track);
  
  /*! Create seed state (at the last measurement) for the backward filtering */
  void CreateBackwardSeedState(TFRFitnode *node);
  
  /*! Update a predicted state with the measurment */
  void UpdateState(TFRFitnode *node, bool forward);
  
  /*! Predict the state at node2 given the state at node1 */
  void PredictState(TFRFitnode *node1, TFRFitnode *node2);
  
  /*! Create smoothed states by averaging forward and backward filtering */
  void SmoothState(TFRFitnode *node);
  
 protected:
  
 private:
  
  /*! The used propagator */
  TFRPropagator *m_propagator;
};

#endif // INCLUDE_TFRPARTICLE_H
