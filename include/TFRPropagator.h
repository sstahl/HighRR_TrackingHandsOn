#ifndef INCLUDE_TFRPROPAGATOR_H
#define INCLUDE_TFRPROPAGATOR_H 1

/*!
 *  @author    Simon Stemmle
 *  @brief     Extrapolator of a track state to a given z position
 */

//C++ libraries
#include <iostream>
#include <stdlib.h>
#include <math.h>

//ROOT libraries
#include "TROOT.h"
#include "TMatrixD.h"

//custom libraries
#include "TFRLayer.h"
#include "TFRState.h"
#include "TFRGeometry.h"

using namespace std;

class TFRPropagator {
 public:

  /*! Empty constructor */
  TFRPropagator(){ };
  
  /*! Standard constructor */
  TFRPropagator(TFRGeometry *_geometry, bool useMultipleScattering = false) :
   m_geo(_geometry), m_useMs(useMultipleScattering) {}

  /*! Destructor */
  ~TFRPropagator( ){ };

  /*! Get the geometry */
  TFRGeometry* GetGeometry() const {return m_geo;};
  
  /*! Get the information wether multiple scattering is taken into account or not */
  bool MultipleScatteringUsed() const {return m_useMs;};
  
  /*! Set wether multiple scattering is taken into account or not */
  void UseMultipleScattering(bool ms) { m_useMs = ms;};

  /*! Extrapolate a state to a given z position. Returns false if the track is extrapolated ouside a detector layer or if the particle is replected by the magnetic field */
  bool PropagateState(TFRState *state, double z);
  
  /*! Extrapolate a state to a given z position without taking into account multiple scattering at layers. Returns false if the track is extrapolated ouside a detector layer or if the particle is replected by the magnetic field */
  bool PropagateStateNoScattering(TFRState *state, double z);
  
  /*! Extrapolate a state to a given z position with a straight line */
  void PropagateStateStraight(TFRState *state, double z);
  
  /*! Extrapolate a state to a given z position in a constant magnetic field. Returns false if the particle is replected by the magnetic field */
  bool PropagateStateInBField(TFRState *state, double z);

  /*! Add the effect of multiple scattering at a certain layer to the covariance matrix of the state */
  bool AddScatteringToCovariance(TFRState *state, TFRLayer *layer);
  
 protected:
  
 private:
  
  /*! The used geometry */
  TFRGeometry *m_geo;

  /*! take into account multiple scattering or not*/
  bool m_useMs;
  
};

#endif // INCLUDE_TFRPROPAGATOR_H
